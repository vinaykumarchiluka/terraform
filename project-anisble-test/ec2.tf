resource "aws_instance" "web" {
  ami               = "ami-0290dbdf20c87600f"
  instance_type     = "t2.micro"
  vpc_security_group_ids = ["sg-08cea16d6c552f757"]
  key_name = "vinay"

  provisioner "local-exec" {
    command = "echo ${self.private_ip} >/tmp/hosts"
  }

  provisioner "remote-exec" {
    connection {
      user = "root"
      password = "DevOps321"
      host = self.private_ip
    }
    inline = ["uptime"]
  }

  provisioner "local-exec" {
    command = <<EOF
      cd /root/ansible1
      ansible-playbook -i /tmp/hosts playbooks/project.yml
EOF
  }

}

provider "aws" {
  region = "us-east-2"
}